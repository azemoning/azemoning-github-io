---
layout: post
title: Web Programming - Module 1
subtitle: Yes, this is my college assignment..
date: 2019-08-24
tags: html
---

Well, hello again!

Today I want to discuss about one of college assignment.  
So this might be a slight tutorial of how I'm working on this assignment.

Okay, without further ado.

Jadi hal yang pertama kita lakukan adalah membuat sebuah folder baru pada direktori tugas masing-masing.  
Contoh:

Windows

```powershell
D:\>mkdir tugas1
D:\>cd tugas1
```

Atau tinggal klik kanan, ```create new folder```

Linux

```bash
mkdir ~/tugas1
cd ~/tugas1
```

Kemudian pada folder/direktori yang sudah dibuat, buat lagi sebuah file bernama ```index.html```

Windows

```powershell
D:\tugas1>copy NUL index.html
```

Atau tinggal klik kanan, ```create new file```

Linux

```bash
touch index.html
```

Nah nantinya file ```index.html``` ini yang akan kita isikan dengan code *HTML*.  

### Membuat kerangka html

Kita sudah membuat sebuah file bernama ```index.html```, sekarang kita coba untuk membuat sebuah kerangkat HTML.  
Kerangkat HTML yang akan kita buat terdiri dari:

```html
<!DOCTYPE html>
```

```<!DOCTYPE>``` bukanlah sebuah tag html, melainkan sebuah instruksi yang diberikan untuk web browser tentang versi kode html yang kita gunakan.  
Walaupun bukan sebuah tag html, tapi ```<!DOCTYPE>``` haruslah selalu ada di baris pertama saat kita menuliskan sebuah kode html.

```html
<html>
    ...
    ...
</html>
```

Tag ```<html>``` merupakan sebuah tag *root* atau akar dari sebuah dokumen html, tag ini nantinya akan memberitahukan web browser bahwa ini adalah dokumen HTML.

```html
<head>
    ...
    ...
</head>
```

Tag ```<head>``` merupakan kontainer dari seluruh elemen *head*, kita dapat mengatur judul dari web browser kita melalui tag ```<title>``` yang berada didalam tag ```<head>```.

```html
<body>
    ...
    ...
</body>
```

Tag ```<body>``` nantinya akan menjadi tag inti yang dimana kita menuliskan konten dari web kita didalam tag ini.

### Membuat judul halaman

Pertama, kita akan membuat judul halaman dari dokumen html kita.  
Untuk membuat sebuah judul dari halaman website, kita perlu menggunakan tag ```<title>``` di dalam tag ```<head>```.

Contoh:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
</html>
{% endhighlight %}

### Mengubah gambar latar belakang

Untuk mengubah latar belakang, baik itu berupa gambar ataupun warna solid, kita dapat menggunakan sebuah attribute dari tag ```<body>``` yang bernama ```background=""```.

Sebelum menuliskan kode html, pastikan kita sudah memiliki gambar yang akan digunakan sebagai latar belakang, contohnya pada kasus ini adalah file gambar bernama ```background.jpg```.  
Jangan lupa untuk meletakkan file gambar ke dalam folder yang sama dengan file ```index.html``` milik kita, agar pengarahan file dalam kode html tidak terlalu panjang.

Contoh penulisan pada html adalah sebagai berikut:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
    </body>
</html>
{% endhighlight %}

### Membuat teks header

Jika kita pernah melakukan pembuatan suatu laporan, biasanya kita akan menggunakan sebuah judul menggunakan header agar memudahkan kita dalam melakukan pembuatan daftar isi nantinya.  
Pada html, kita dapat membuat sebuah teks header dengan menggunakan tag ```<h1>``` hingga ```<h6>``` (yang terbesar dimulai dari angka 1).  

Contoh pembuatan teks header pada html adalah seperti berikut:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1>Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
    </body>
</html>
{% endhighlight %}

Atau jika ingin teks header berada di tengah, kita dapat menggunakan attribute ```align="center"```

{% highlight html%}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1 align="center">Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
    </body>
</html>
{% endhighlight %}

### Membuat garis horizontal

Untuk menandakan suatu bagian dari konten, atau hanya untuk mempercantik tampilan website kita, kita dapat menggunakan tag ```<hr>``` untuk membuat sebuah garis horizontal dari ujung kiri tampilan hingga ujung kanan.

Contoh pembuatan garis horizontal:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1 align="center">Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
        <hr>
    </body>
</html>
{% endhighlight %}

Tag ```<hr>``` memiliki attribute ```size``` dan ```color``` untuk menentukan ketebalan garis dan warna dari garis tersebut.

Contoh pembuatan garis horizontal menggunakan attribute ```size``` dan ```color```:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1 align="center">Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
        <hr size="5" color="brown">
    </body>
</html>
{% endhighlight %}

### Membuat paragraf

Untuk membuat sebuah paragraf pada dokumen html, kita dapat menggunakan tag ```<p>```, perbedaan dari kita menuliskan suatu konten menggunakan tag ```<p>``` adalah, web browser akan secara otomatis menambahkan margin sebelum dan setelah tag ```<p>```.

Contoh pembuatan paragraf:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1 align="center">Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
        <hr size="5" color="brown">
        <p>
            Penangkaran My Pack of Wolves pada situsnya menjelaskan bahwa  
            meskipun serigala dan anjing memiliki DNA yang nyaris sama,  
            serigala tidak direkomendasikan untuk dipelihara oleh orang awam.  
            Perilaku serigala tidak dapat diprediksi karena sifat liarnya,  
            sementara anjing telah lama hidup bersisian dengan manusia.
        </p>
    </body>
</html>
{% endhighlight %}

Tag ```<p>``` juga memiliki beberapa attribute, salah satunya adalah ```align``` yang berfungsi untuk membuat teks paragraf menjadi rata, baik itu kiri atau ```left```, tengah atau ```center```, kanan atau ```right```, maupun kiri dan kanan atau ```justify```.

Contoh pembuatan paragraf dengan attribute ```align```:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1 align="center">Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
        <hr size="5" color="brown">
        <p align="justify">
            Penangkaran My Pack of Wolves pada situsnya menjelaskan bahwa  
            meskipun serigala dan anjing memiliki DNA yang nyaris sama,  
            serigala tidak direkomendasikan untuk dipelihara oleh orang awam.  
            Perilaku serigala tidak dapat diprediksi karena sifat liarnya,  
            sementara anjing telah lama hidup bersisian dengan manusia.
        </p>
    </body>
</html>
{% endhighlight %}

### Mengubah tipe dan warna font

Untuk mengkustomisasi font pada dokumen html kita, kita dapat menggunakan tag ```<font>``` dan tag ```<font>``` memiliki beberapa attribute, contohnya jika kita ingin merubah tipe font dan warna font, kita dapat menggunakan attribute ```face``` dan ```color```.

Contoh penggunaan tag ```<font>``` untuk mengubah tipe dan warna font:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1 align="center">Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
        <hr size="5" color="brown">
        <p align="justify">
            Penangkaran My Pack of Wolves pada situsnya menjelaskan bahwa  
            meskipun serigala dan anjing memiliki DNA yang nyaris sama,  
            serigala tidak direkomendasikan untuk dipelihara oleh orang awam.  
            Perilaku serigala tidak dapat diprediksi karena sifat liarnya,  
            sementara anjing telah lama hidup bersisian dengan manusia.
        </p>
        <p align="center">
            <font face="arial" color="red">
                    Serigala akan menyerang bila merasa terancam,  
                    kelaparan, terdorong oleh sifat agresif,  
                    dan untuk bertahan diri.  
                    Usaha manusia untuk mendisiplinkan atau  
                    menjinakkan serigala juga dapat berakibat fatal.
            </font>
        </p>
    </body>
</html>
{% endhighlight %}

### Pengelompokkan atau divisi suatu konten

Kita dapat mengelompokkan beberapa tag html yang berisi konten tertentu menjadi satu divisi, sehingga nantinya kita dapat lebih mudah untuk memanipulasi konten tersebut berbeda dengan konten yang lain. Untuk melakukan hal tersebut kita dapat menggunakan tag ```<div>``` pada html.

Contoh penggunaan tag ```<div>```:

{% highlight html%}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1 align="center">Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
        <hr size="5" color="brown">
        <p align="justify">
            Penangkaran My Pack of Wolves pada situsnya menjelaskan bahwa  
            meskipun serigala dan anjing memiliki DNA yang nyaris sama,  
            serigala tidak direkomendasikan untuk dipelihara oleh orang awam.  
            Perilaku serigala tidak dapat diprediksi karena sifat liarnya,  
            sementara anjing telah lama hidup bersisian dengan manusia.
        </p>
        <p align="center">
            <font face="arial" color="red">
                    Serigala akan menyerang bila merasa terancam,  
                    kelaparan, terdorong oleh sifat agresif,  
                    dan untuk bertahan diri.  
                    Usaha manusia untuk mendisiplinkan atau  
                    menjinakkan serigala juga dapat berakibat fatal.
            </font>
        </p>
        <div>
            //konten
        </div>
    </body>
</html>
{% endhighlight %}

Tag ```<div>``` juga memiliki beberapa attribute, salah satunya jika kita ingin membuat posisi dari konten yang berada dalam tag ```<div>``` menjadi rata kanan,
kita dapat menggunakan attribute ```align```.

Contoh penggunaan tag ```<div>``` menggunakan attribute ```align```:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1 align="center">Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
        <hr size="5" color="brown">
        <p align="justify">
            Penangkaran My Pack of Wolves pada situsnya menjelaskan bahwa  
            meskipun serigala dan anjing memiliki DNA yang nyaris sama,  
            serigala tidak direkomendasikan untuk dipelihara oleh orang awam.  
            Perilaku serigala tidak dapat diprediksi karena sifat liarnya,  
            sementara anjing telah lama hidup bersisian dengan manusia.
        </p>
        <p align="center">
            <font face="arial" color="red">
                    Serigala akan menyerang bila merasa terancam,  
                    kelaparan, terdorong oleh sifat agresif,  
                    dan untuk bertahan diri.  
                    Usaha manusia untuk mendisiplinkan atau  
                    menjinakkan serigala juga dapat berakibat fatal.
            </font>
        </p>
        <div align="right">
            //konten
        </div>
    </body>
</html>
{% endhighlight %}

### Memasukkan gambar  

Kita dapat memasukkan gambar pada dokumen html kita dengan menggunakan tag ```<img>```, pada tag ```<img>``` kita wajib mengisikan menambahkan attribute ```src``` dan menuliskan nama file gambar yang ingin kita tampilkan pada attribute tersebut.

Sebelum menuliskan kode html, pastikan kita sudah memiliki file gambar yang akan ditampilkan, contohnya pada kasus ini adalah file gambar bernama ```wolf.jpeg```.
Jangan lupa untuk meletakkan file gambar ke dalam folder yang sama dengan file index.html milik kita, agar pengarahan file dalam kode html tidak terlalu panjang.

Contoh penggunaan tag ```<img>``` untuk memasukkan gambar:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1 align="center">Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
        <hr size="5" color="brown">
        <p align="justify">
            Penangkaran My Pack of Wolves pada situsnya menjelaskan bahwa  
            meskipun serigala dan anjing memiliki DNA yang nyaris sama,  
            serigala tidak direkomendasikan untuk dipelihara oleh orang awam.  
            Perilaku serigala tidak dapat diprediksi karena sifat liarnya,  
            sementara anjing telah lama hidup bersisian dengan manusia.
        </p>
        <p align="center">
            <font face="arial" color="red">
                    Serigala akan menyerang bila merasa terancam,  
                    kelaparan, terdorong oleh sifat agresif,  
                    dan untuk bertahan diri.  
                    Usaha manusia untuk mendisiplinkan atau  
                    menjinakkan serigala juga dapat berakibat fatal.
            </font>
        </p>
        <div align="right">
            <img src="wolf.jpeg">
        </div>
    </body>
</html>
{% endhighlight %}

Jika kita hanya menggunakan attribute ```src```, maka ukuran gambar yang masuk adalah ukuran gambar asli, tentunya akan membuat tampilan html kita menjadi kurang cantik. Untuk itu, kita perlu menggunakan attribute ```width``` untuk mengatur lebar gambar dan diisikan dengan angka (misalnya: ```200```, dan dengan satuan pixel yaitu ```px```), dan jika kita ingin mempercantik gambar kita, kita juga dapat menggunakan attribute ```border``` untuk memberi garis tepi seperti bingkai pada gambar.

Contoh penggunaan tag ```<img>``` menggunakan attribute ```width``` dan ```border```:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1 align="center">Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
        <hr size="5" color="brown">
        <p align="justify">
            Penangkaran My Pack of Wolves pada situsnya menjelaskan bahwa  
            meskipun serigala dan anjing memiliki DNA yang nyaris sama,  
            serigala tidak direkomendasikan untuk dipelihara oleh orang awam.  
            Perilaku serigala tidak dapat diprediksi karena sifat liarnya,  
            sementara anjing telah lama hidup bersisian dengan manusia.
        </p>
        <p align="center">
            <font face="arial" color="red">
                    Serigala akan menyerang bila merasa terancam,  
                    kelaparan, terdorong oleh sifat agresif,  
                    dan untuk bertahan diri.  
                    Usaha manusia untuk mendisiplinkan atau  
                    menjinakkan serigala juga dapat berakibat fatal.
            </font>
        </p>
        <div align="right">
            <img src="wolf.jpeg" width="255px" border="5">
        </div>
    </body>
</html>
{% endhighlight %}

### Final

Sekarang jika kita menjalankan file ```index.html``` kita, maka kita sudah dapat melihat tampilan yang sesuai dengan apa yang diminta pada tugas praktikum modul 1.

Kode HTML secara utuh:

{% highlight html %}
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Praktikum 1</title>
    </head>
    <body background="background.jpg">
        <h1 align="center">Serigala - Hewan Eksotis Bebas dan Berbahaya</h1>
        <hr size="5" color="brown" />
        <p align="justify">
            Penangkaran My Pack of Wolves pada situsnya menjelaskan bahwa 
            meskipun serigala dan anjing memiliki DNA yang nyaris sama, 
            serigala tidak direkomendasikan untuk dipelihara oleh orang awam. 
            Perilaku serigala tidak dapat diprediksi karena sifat liarnya, 
            sementara anjing telah lama hidup bersisian dengan manusia.
        </p>
        <p align="justify">
            Serigala memang berbahaya bagi manusia, 
            tetapi frekuensi serangan serigala terbilang jarang. 
            Ini disebabkan karena serigala hidup jauh dari manusia 
            dan memiliki kecenderungan kuat untuk menghindari manusia.
        </p>
        <p align="center">
            <font face="arial" color="red">
                <b>
                    Serigala akan menyerang bila merasa terancam, 
                    kelaparan, terdorong oleh sifat agresif, 
                    dan untuk bertahan diri. 
                    Usaha manusia untuk mendisiplinkan atau 
                    menjinakkan serigala juga dapat berakibat fatal.
                </b>
            </font>
        </p>
        <hr size="5" color="grey" />
        <div align="right">
            <br />
            <hr align="right" size="3" width="255px" color="red"/>
            <img src="wolf.jpeg" width="250px" border="5"/>
        </div>
    </body>
</html>
{% endhighlight %}

Untuk melihat contoh tampilan dari kode HTML diatas, bisa klik link berikut:

- [Tugas 1](https://blog.saufi.me/assets/web-programming-module-1/tugas1/)
- [Tugas 2](https://blog.saufi.me/assets/web-programming-module-1/tugas2/)
- [Tugas 3](https://blog.saufi.me/assets/web-programming-module-1/tugas3/)

See ya!
