---
layout: page
title: About me
subtitle: Why you'd want to go on a date with me
---

My name is Ahmad Saufi Maulana, or you can call me Upi. I have the following qualities:

- I rock a great mustache.
- I'm extremely loyal to my family and friends!
- I'm awesome, of course.

What else do you need?

### my history

To be honest, I'm having some trouble remembering right now, so why don't you just watch [my movie](https://www.imdb.com/title/tt0088323/) and it will answer **all** your questions.
